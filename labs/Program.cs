﻿using System;

namespace labs
{
    class Employee
    {
        private string fullName;
        private int departmentNumber;
        private string post;
        private string workExperience;

        public string FullName { get => fullName; set => fullName = value; }
        public int DepartmentNumber { get => departmentNumber; set => departmentNumber = value; }
        public string Post { get => post; set => post = value; }
        public string WorkExperience { get => workExperience; set => workExperience = value; }


        public override string ToString()
        {
            return String.Format("ФИО: {0}, Номер отдела: {1}, Должность: {2}, Стаж работы: {3}", FullName, DepartmentNumber, Post, WorkExperience);
        }

        public override bool Equals(object obj)
        {
            Employee emp = (Employee)obj;
            return (emp.DepartmentNumber == DepartmentNumber);
        }

    }

    class TestEmployee
    {
        public static void LoadTestEmployee()
        {
            Employee employee1 = new Employee()
            {
                FullName = "Некрасов Сергей Артурович",
                DepartmentNumber = 3,
                Post = "Инженер-программист",
                WorkExperience = "2.5 года"
            };

            Employee employee2 = new Employee()
            {
                FullName = "Громов Дмитрий Николаевич",
                DepartmentNumber = 5,
                Post = "Тестировщик",
                WorkExperience = "1 год"
            };
            Employee employee3 = new Employee()
            {
                FullName = "Волков Григорий Жаров",
                DepartmentNumber = 5,
                Post = "Инженер-программист",
                WorkExperience = "3 года"
            };

            Display("employee1", employee1);
            Display("employee2", employee2);
            Display("employee3", employee3);
        }

        private static void Display(string label, Employee employee)
        {
            Console.WriteLine("----{0}----", label);
            Console.WriteLine("Данные о сотруднике: {0}", employee.ToString());

        }

        private static void CompareEmployeeObject(Employee emp1, Employee emp2)
        {
            Console.WriteLine("Equals() = {0}", emp1.Equals(emp2));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TestEmployee.LoadTestEmployee();
            Console.ReadLine();
        }
    }
}
